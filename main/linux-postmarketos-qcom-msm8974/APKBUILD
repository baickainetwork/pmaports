# Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.11.10
pkgrel=0
_commit="901cb33cc2ea167dbb1de7032815ea65fd49d81f"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	findutils
	flex
	gmp-dev
	installkernel
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	"
source="https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/$_commit/linux-postmarketos-$_commit.tar.gz
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-postmarketos-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="27393de29fb1e08867f6572194625453a220cc0fb2d884c73da7f12ebef86912bf96ba1cac881639e44b6b3da9d3cbd44951488b2e019f5b0d597b284588f39f  linux-postmarketos-901cb33cc2ea167dbb1de7032815ea65fd49d81f.tar.gz
19846a0d7c0e51de89bdfd517acaebc94a1478f0e7e1ff8d7666e7d9cc5a0dc5d7edf09fb413aeb37448aabe80b5457c6c07ece9f825c9507110b1588e7ce5a2  config-postmarketos-qcom-msm8974.armv7"
